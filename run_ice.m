clear all
up = [10e-3];
for i = 1:1
    [time,glacier,mprd,merupt,incCMelt,topCMelt,carbonerupt,Fmean,Fmax,phi,T,coord] = melt1d_TVD_ice(10e3,10^21,200e3,1450,up(i));
    filename = sprintf('test.mat',1450,up(i));
    save(filename)
end

% for i = 1:1
%     [time,glacier,mprd,merupt,incCMelt,topCMelt,carbonerupt,phi,T,coord] = melt1d_TVD_ice(10e3,10^21,200e3,1450,10e-3);
%     filename = sprintf('data/20kyr/1e-7_10^21_%d_200_10mmyr_Te10_k1e-5_40NMORB.mat',1450);
%     save(filename)
% end
