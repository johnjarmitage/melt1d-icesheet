% 1-D mixed FD/FV_TVD/FEM that couples surface flexure with melt production 
%            John Armitage (IPGP)
%            Thijs Franken
%            Nobu Fuji
% Help from: Stefan Schmalholz (Unil)
%            Jason Morgan (RHUL)
%            Benjamin Campforts (Leuven)
%            Mustapha Zakari (IPGP)

% [time,glacier,mprd,merupt,incCMelt,topCMelt,carbonerupt,phi,T,coord] = melt1d_TVD_ice(10e3,10^20.5,200e3,1450,30e-3);

function [time,glacier,mprd,merupt,incCMelt,topCMelt,carbonerupt,Fmean,Fmax,phi,T,coord,xcoord,ice] = melt2d_TVD_ice(Te,eta,width,Tp,vel)
global zMin zMax NN params NNflex X Xmid;
%close all force

plotting  = 'off';
plotend   = 'on';
ice_layer = 'on';
ice_onoff = 'Tim120';
REE_comp  = 'on';

tstep = 1e4;

setupProblem();
setupParams(tstep,Tp,vel);

setupFlexProblem()
setupFlexParams(tstep,Te,eta,width);
setupFlexLocMatrix();

if (strcmp(plotting,'on') == 1)
    fig1 = figure(201);
    set(fig1,'position',[10 10 1000 600]);
end

% some pseudo-2D parameters
Nice      = 6;
Nleft     = (NNflex+1)/2+1; % just to test
Nright    = Nleft+Nice;
angle     = 45/180*pi;
xcoord    = X(Nleft:Nright)-min(X(Nleft:Nright));
NX        = length(xcoord);
zztop     = -abs(tan(angle)*(xcoord-X((NNflex+1)/2))*params.lx); % dimensional

% allocate arrays
T          = zeros(NN,NX); % temperature
T_old      = zeros(NN,NX); % temperature
dFdP       = zeros(NN,NX); % melt production
Dep        = zeros(NN,NX); % depletion
Xwater     = params.water*ones(NN,NX); % depletion
phi        = zeros(NN,NX); % porosity
velphi     = zeros(NN,NX); % porosity velocity
velphiC    = zeros(NN,NX); % melt velocity
velS       = zeros(NN,NX); % solid velocity

latentheat = zeros(NN,NX);

w         = zeros(NNflex*2,1);
q         = zeros(NNflex,1);

% Flexure matricies
start_diag = sparse(diag(ones(NNflex*2+1,1))+diag(ones(NNflex*2,1),1)+diag(ones(NNflex*2,1),-1));
MM = sparse(start_diag(1:NNflex*2,1:NNflex*2));
KK = sparse(start_diag(1:NNflex*2,1:NNflex*2));

% Calculate coordinates
range = zMax - zMin;
dz     = range / (NN-1);
for i = 1:NN
    coord(i) = zMin + (i-1)*dz;
end
Pres   = params.rhom*params.g*params.lz*coord';

% initial composition
if (strcmp(REE_comp,'on') == 1)
    [CMantle,CMelt,nREE] = setupComp(NX);
else
    Cmelt  = 0;
end

% initial condition
totphi      = 0;
Ti          = ones(NN,1);
%T          = linspace(0,4,NN)';
Ts          = get_Ts(Pres,Dep(:,1));
Twet        = (Ts + params.aX.*(0.01.*Xwater(:,1)).^params.bX)./params.T0;
Ti(Ti>Twet) = Twet(Ti>Twet);
Ti(Ti>1)    = 1;
T           = repmat(Ti,1,NX);

% initial ice
change_ice = params.steplength;

% upwards mean velocity
Vel = corner_flow(params.vel,xcoord,coord,params.lx,params.lz,NN);
% Vel = params.vel*ones(NN,NX);  % uniform upwards velocity%
disp(params.vel)

% figure(1)
% plot(coord,Vel(:,1))
% drawnow
    
% Perform itterations
params.iterate  = 1; % global

t        = 0;
w_use    = zeros(NX,1);
tmax     = params.max_time;
tmaxt    = round(tmax) + 1;
time     = zeros(1,tmaxt);
mprd     = zeros(1,tmaxt,NX);
merupt   = zeros(1,tmaxt,NX);
Fmean    = zeros(1,tmaxt,NX);
Fmax     = zeros(1,tmaxt,NX);
if (strcmp(REE_comp,'on') == 1)
    incCMelt = zeros(nREE,tmaxt,NX);
    topCMelt = zeros(nREE,tmaxt,NX);
    Cerupt   = zeros(nREE,tmaxt,NX);
    totCPhi  = zeros(nREE,NX);
end
ice      = zeros(NX,tmaxt);


if (strcmp(ice_onoff,'Tim120') == 1)
    tim   = load('iceland_rescaled_120k.mat');
    Nrift = [1e-3*tim.IcelandV.time 1800*tim.IcelandV.ICE5G_res_idx];
    Nrift(Nrift<0) = 0;
    %Nrift(isnan(Nrift)) = 0;
    max_time_kyrs = params.lz*params.lz*params.max_time/params.kappa/(365*24*60*60)*1e-3;
    Nrift = [Nrift; [max_time_kyrs 0]];
    Nrift(:,1) = max_time_kyrs - Nrift(:,1);
%     % modification to make zero ice thickness < 10 ka
%     [n10ka,~] = find(Nrift(:,1)>4990);
%     Nrift(n10ka,2) = 0;
end

onoff    = 1;
while params.iterate > 0
    
    t = t+1;

    dt = params.dt;
    if t == 1
        time(t) = dt;
    else
        time(t) = time(t-1) + dt;
    end
      
    % calculate melt velocity for 'horrible advection'
    % velphi = params.cporo*2*phi.*(1-(3/2)*phi); % n = 2 in permeability
    velphi  = params.cporo*3*phi.*phi.*(1-(4/3)*phi); % n = 3 in permeability
    % velphi(phi>1e-2) = 100*velphi(phi>1e-2);
    
    % velocity for melt composition
    velphiC = params.cporo*phi.*phi.*(1-phi) + Vel; % n = 3 in permeability
    
    % velocity for solid composition
    velS = (Vel - phi.*(params.cporo*phi.*phi.*(1-phi) + Vel))./(1-phi);
    
    % Ice, flexure and stability
    if (strcmp(ice_layer,'on') == 1)
        w_use_old = w_use;
        been_here = 0;
        get_out_of_here = 0;
        while get_out_of_here == 0
            if (strcmp(ice_onoff,'step') == 1)
                if (time(t) > change_ice)
                    onoff  = -1*onoff;
                    change_ice = time(t) + params.steplength;
                end
                glacier(t) = params.rhoi*params.g*1e3*(1 + onoff); % ice load
            elseif (strcmp(ice_onoff,'step2') == 1)
                if (mod(t,params.steplength_on) == 0)
                    onoff  = -1;
                    ttemp  = t;
                end
                if (t > params.steplength_on)
                    if (t > ttemp + params.steplength_off)
                        onoff  = 1;
                    end
                end
                glacier(t) = params.rhoi*params.g*1e3*(1 + onoff); % ice load
            elseif (strcmp(ice_onoff,'step1') == 1)
                onoff  = -1;
                if (time(t) > params.steplength_on && time(t) < params.steplength_off)
                    onoff  = 1;
                end
                glacier(t) = params.rhoi*params.g*1e3*(1 + onoff); % ice load
            elseif (strcmp(ice_onoff,'sin') == 1)
                glacier(t) = params.rhoi*params.g*1e3*(1 + sin(2*params.periodicity*pi*(params.myr*time(t))-2*params.periodicity*pi));
            elseif (strcmp(ice_onoff,'Tim120') == 1)
                icethick = interp1(1e3*Nrift(:,1),Nrift(:,2),params.lz*params.lz*time(t)/params.kappa/(365*24*60*60));
                glacier(t) = params.rhoi*params.g*icethick;
            end
            
            % Calculate the flexure for time step dt
            flexstep      = dt*params.lz*params.lz/params.kappa; %time step in seconds
            [w_new,w_out] = flexure(w,glacier(t),flexstep/params.tscale);
            w_use         = w_out(NNflex:2:NNflex+2*Nice); % need to extract a range of flexure
            ice(:,t)      = (w_use-w_use_old)/params.lz; % positive is down and make dimensionless
                        
            % Equation 29 England et al, JGR 1985
            wavelength = 2*params.lambdaload*params.lx/params.lz;
            ice_disp   = ice(:,t)*exp(-sqrt(3)*pi*(coord-zMin)/wavelength); %.*(1-2*pi*(coord-zMin)/wavelength);
            
            % No dispersion of the displacement.
            % ice_disp   = ice(t)*ones(1,NN);
            
            % check cfl for advection and diffusion
            cfl = 0.5;
            dtc = cfl*(dz*dz/((max(abs(params.vel))+max(abs(ice_disp(:)))/dt)*dz + 2));
            dtp = cfl*(dz*dz/((max(abs(velphi(:)))+max(abs(ice_disp(:)))/dt)*dz + 2));
            dt3 = cfl*(dz*dz/((max(abs(velphiC(:)))+max(abs(ice_disp(:)))/dt)*dz + 2));
            dtf = dt;
            dt  = min([dt dtc dtp dt3]);
            if (been_here == 1)
                get_out_of_here = 1;
            end
            if (abs(dtf-dt)/dtf < 1e-6)
                been_here = 1;
            end
        end
        w = w_new;
    else
        ice_disp   = zeros(NX,1);
        
        % check cfl for advection and diffusion
        cfl = 0.5; % small number, I know...
        dtc = cfl*(dz*dz/(max(abs(params.vel))*dz + 2));
        dtp = cfl*(dz*dz/((max(abs(velphi(:)))+max(abs(ice_disp(:)))/dt)*dz + 2));
        dt3 = cfl*(dz*dz/((max(abs(velphiC(:)))+max(abs(ice_disp(:)))/dt)*dz + 2));
        dt  = min([dt dtc dtp dt3]);
    end
    
    infot = sprintf('dt %.3e, dtc %.3e, vel %.3e, velphi %.3e, velphiC %.3e, velS %.3e\n',...
        dt,min([dtc dtp dt3]),...
        max(abs(params.vel+ice_disp(:)/dt)),max(abs(velphi(:)))+max(abs(ice_disp(:)))/dt,...
        max(abs(velphiC(:)))+max(abs(ice_disp(:)))/dt,max(abs(velS(:)))+max(abs(ice_disp(:)))/dt);
    infod(t) = max(velphi(:));
    disp(infot);
    
    for ix = 1:NX
        
        % First advect with mean velocity
        T(:,ix)      = advection_1d(coord,T(:,ix),Vel(:,ix),ice_disp(ix,:)',dz,dt,'upwind');
        phi(:,ix)    = advection_1d(coord,phi(:,ix),Vel(:,ix),ice_disp(ix,:)',dz,dt,'TVD');     % simple advection
        
        % First advect aditionally porosity
        phi(:,ix)    = advection_1d(coord,phi(:,ix),velphi(:,ix),ice_disp(ix,:)',dz,dt,'TVD'); % horrible advection
        
        % First advect solids
        Dep(:,ix)    = advection_1d(coord,Dep(:,ix),velS(:,ix),ice_disp(ix,:)',dz,dt,'TVD');
        Xwater(:,ix) = advection_1d(coord,Xwater(:,ix),velS(:,ix),ice_disp(ix,:)',dz,dt,'TVD');
        
        if (strcmp(REE_comp,'on') == 1)
            for i = 1:nREE
                CMantle(i,:,ix) = advection_1d(coord,CMantle(i,:,ix)',velS(:,ix),ice_disp(ix,:)',dz,dt,'TVD');
                CMelt(i,:,ix) = advection_1d(coord,CMelt(i,:,ix)',velphiC(:,ix),ice_disp(ix,:)',dz,dt,'TVD');
            end
        end
        
        % Calculate amount of erupted melt and its composition
        %     merupt(t) = (totphi - trapz(phi)*dz)/dt;
        %     if (strcmp(REE_comp,'on') == 1)
        %         for i = 1:nREE
        %             Cerupt(i,t) = totCPhi(i) - trapz(CMelt(i,:).*phi')/trapz(phi);
        %         end
        %     end
        
        % Second diffusion
        T(:,ix) = diffusion_1d(T(:,ix),dz,dt);
        
        % Calculate melt
        [T(:,ix),dFdP(:,ix),Dep(:,ix),phi(:,ix),Xwater(:,ix)] = meltingRK(T(:,ix),Pres,Dep(:,ix),phi(:,ix),Xwater(:,ix),dt);
        %     [T,dFdP,latentheat,Xwater] = melting(T,Pres,Dep,Xwater);
        %     dDep   = dt*params.rhom*params.g*abs(params.vel).*dFdP;
        %     Dep    = Dep + dDep; % melt produced in one time step
        
        % Calculate melt production/eruption rate
        mprd(t,ix) = abs(params.vel)*trapz(dFdP(:,ix))*dz;
        %     totphi = trapz(phi)*dz;
        merupt(t,ix) = phi(1,ix)*abs(velphiC(1)-velS(1));
        
        % Calculate Fmean and Fmax
        Fmax(t,ix) = max(Dep(:,ix));
        Fmean(t,ix) = trapz(Dep(:,ix).*dFdP(:,ix))/trapz(dFdP(:,ix));
        
        % Update composition
        if (strcmp(REE_comp,'on') == 1)
            [CMantle(:,:,ix),CMelt(:,:,ix),Dmelt(:,:,ix)] = REEcomp(T(:,ix),Pres,coord,dFdP(:,ix),CMantle(:,:,ix),CMelt(:,:,ix),phi(:,ix));
            for i = 1:nREE
                incCMelt(i,t,ix) = trapz(CMelt(i,:,ix).*dFdP(:,ix)')/trapz(dFdP(:,ix));
                % totCPhi(i) = trapz(CMelt(i,:).*phi')/trapz(phi);
                topCMelt(i,t,ix) = CMelt(i,1,ix);
            end
            carbonerupt(t,ix) = CMelt(15,1,ix)*abs(velphiC(1)-velS(1));
        end
        
        % Subtract latent heat of melting
        % T = T - dt*params.rhom*params.g*latentheat.*abs(params.vel).*dFdP/params.T0;
        
        % done now?
    end
    
    % I guess so, so lets update time in case dt changed
    if t == 1
        time(t) = dt;
    else
        time(t) = time(t-1) + dt;
    end
    if time(t) > tmax
        params.iterate = 0;
    end
%     if t > 2000
%         params.iterate = 0;
%     end
    
    if t == 1
        Ts      = get_Ts(Pres,Dep(:,1));
        Ts_wet2 = Ts + params.aX.*(0.01.*Xwater(:,1)).^params.bX;
    end
    if (mod(t,10) == 0 && strcmp(plotting,'on') == 1)
        figure(fig1)
        subplot(1,5,1)
        plot(params.T0*T,1e-3*params.lz*coord,'r');
        hold on
        Ts      = get_Ts(Pres,Dep(:,1));
        Ts_wet1 = Ts + params.aX.*(0.01.*Xwater(:,1)).^params.bX;
        plot(Ts_wet1,1e-3*params.lz*coord,'k');
        hold off
        set(gca,'ylim',[1e-3*params.lz*min(coord) 150]);
        xlabel('Temperature (^{\circ}C)');
        ylabel('Depth (km)');
        axis ij
        drawnow;
             
        subplot(1,5,2)
        plot(phi,1e-3*params.lz*coord);
        %set(gca,'xlim',[0 0.02]);
        set(gca,'ylim',[1e-3*params.lz*min(coord) 150]);
        xlabel('\phi');
        ylabel('Depth (km)');
        axis ij
        drawnow;
        
        if (strcmp(REE_comp,'on') == 1)
            subplot(1,5,3:5)
            plot(CMantle(:,:,6),1e-3*params.lz*coord);
            set(gca,'xscale','log')
            set(gca,'ylim',[1e-3*params.lz*min(coord) 150]);
            xlabel('REEs (ppm)');
            ylabel('Depth (km)');
            legend('La','Ce','Pr','Nd','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu')
            axis ij
            drawnow;
        end
        
        printname = sprintf('plots/simple_mod_%05d.png',t);
        print(fig1,'-dpng',printname)
    end
    
end

if (strcmp(plotting,'on') == 1)
    figure(fig1)
    subplot(1,6,1)
    hold on
    %plot(Ts_wet1,1e-3*params.lz*coord,'b');
    hold off
end

if (strcmp(plotend,'on') == 1)
    fig2 = figure(102);
    set(fig2,'position',[12 12 600 600]);
    subplot(1,2,1)
    hold on, plot(params.T0*T(:,1),1e-3*params.lz*coord,'r');
    plot(get_Ts(Pres,Dep(:,1)),1e-3*params.lz*coord,'k--');
    plot(get_Ts(Pres,Dep(:,1)) + params.aX.*(0.01.*Xwater(:,1)).^params.bX,1e-3*params.lz*coord,'k');
    set(gca,'xlim',[800 1600]);
    set(gca,'ylim',[1e-3*params.lz*min(coord) 150]);
    xlabel('Temperature (^{\circ}C)');
    ylabel('Depth (km)');
    axis ij
    
    subplot(1,2,2)
%    plot(Vel,1e-3*params.lz*coord);
    plot(phi,1e-3*params.lz*coord,'k');
    set(gca,'xlim',[0 0.01]);
    set(gca,'ylim',[1e-3*params.lz*min(coord) 150]);
    xlabel('\phi');
    ylabel('Depth (km)');
    axis ij
    
end

end





%========================================================================
% MELTING
%========================================================================
function [] = setupProblem
%========================================================================
global zMin zMax NN
zMin = 0.1;
zMax = 1;
NN   = 1001;

end % End of function setupProblem()

%========================================================================
function [] = setupParams(tstep,Tp,vel)
%========================================================================
global params;

year   = 365*24*60*60;
w      = vel/year;  % half spreading rate
tstep  = tstep*year;
tmax   = 5e6*year;

params.lz     = 300e3;   % system depth metres
params.T0     = Tp;
params.g      = 9.8;
params.kappa  = 1e-6;  % thermal diffusion coefficient
params.Cp     = 1.2e3; % heat capacity
params.deltaS = 400;   % entropy change upon1 melting
params.alpha  = 3e-5;  % thermal activation energy
params.rhom   = 3100;  % mantle density
params.rhoi   = 920;   % ice density
params.rhol   = 2800;  % melt density

params.Tsub   = 200;
params.L      = 550;
params.Fdry   = 0.02;
params.dF     = 1e-6;

% porosity constant
permeability  = 1e-5; % permeability (m^2)
etaf          = 1;    % shear viscosity of melt (Pas)
params.cporo  = -(params.lz/params.kappa)*(params.rhom-params.rhol)*params.g*permeability/etaf;

% Coefficients for parameterization of wet melting (Katz et al. 2003)
params.Ts0     = 1081;
params.dTdP_F  = 132*1e-9;  % solidus pressure gradient 
params.dTdF_Ps = 800;       % solidus depletion gradient spinel lherzolite
params.dTdF_Ph = 800;       % solidus depletion gradient harzburgite
params.water   = 200;
params.Dbulk   = 0.007;     % partition coefficient for water
params.aX      = -43;
params.bX      = 0.75;

params.vel      = params.lz*w/params.kappa;  % spreading rate
params.dt       = params.kappa*tstep/(params.lz*params.lz);
params.max_time = params.kappa*tmax/(params.lz*params.lz);

% more ice stuff
params.periodicity    = 1/0.1; % 1/millions of years
params.steplength     = params.kappa*2e4*year/(params.lz*params.lz);
% params.steplength_on  = round(4e6*year/tstep);
% params.steplength_off = round(5e6*year/tstep);
params.steplength_on  = 0; %params.kappa*5e6*year/(params.lz*params.lz);
params.steplength_off = params.kappa*10e6*year/(params.lz*params.lz);
params.myr            = 1e-6*params.lz*params.lz/(params.kappa*year);
params.halfdepth      = 0.01;  % dimensionless depth at which the displacement reduces by 1/2

end

%========================================================================
function vari = advection_1d(x,var,U,w,dz,dt,method)
%========================================================================
global NN params
if strcmp(method,'upwind') == 1 
    vel = (U + w/dt)';
    A   = sparse(diag((1+dt/dz*vel(1:NN))) +...
          diag(-dt/dz*vel(1:NN-1),1));
    % boundary conditions
    % top zero gradient
    
    % top fixed value
    A(1,1)   = 1;
    A(1,2)   = 0;
    % bottom fixed value
    A(NN,NN) = 1;
    vari     = A*var;
elseif strcmp(method,'TVD') == 1
    % lets be general and assume we don't know if vel>0
    vel   = (U + w/dt);
    
    % ghost cells required of my artificial boundary conditions:
    % non-reflecting Neumann type boundary conditions are implemented
    vargh = [var(1); var; var(NN)];
    velgh = [vel(1); vel; vel(NN)];
    theta = ones(NN+2,1);
    theta(velgh<0) = -1;
    
    % calculate slopes for the flux limiter (phi)
    TVD_r     = vargh(2:end);
    TVD_r2    = [vargh(3:end); vargh(end)];
    TVD_m     = vargh(1:end-1);
    TVD_l     = [vargh(1); vargh(1:end-2)];
    
    r_TVDup   = (TVD_r2-TVD_r)./(TVD_r-TVD_m);
    r_TVDdown = (TVD_m-TVD_l)./(TVD_r-TVD_m);
    
    r_TVD = r_TVDdown;
    r_TVD(theta(2:end)<0) = r_TVDup(theta(2:end)<0);
    r_TVD(diff(TVD_m)==0) = 1;
    r_TVD(1) = 1;
    r_TVD(end) = 1;
    
%     l_TVDup    = (TVD_m-TVD_l)./(TVD_r-TVD_m);
%     l_TVDdown  = (TVD_r2-TVD_r)./(TVD_r-TVD_m);
%     
%     l_TVD = l_TVDdown;
%     l_TVD(theta(1:end-1)<0) = l_TVDup(theta(1:end-1)<0);
%     l_TVD(diff(TVD_r)==0) = 1;
               
    % define Flux Limiter function (Van Leer)
    phi = (r_TVD + abs(r_TVD))./(1 + abs(r_TVD));
%    phi_l = (l_TVD + abs(l_TVD))./(1 + abs(l_TVD));
    phi_r = phi(2:end);
    phi_l = phi(1:end-1);
    
    % think about my ghost cells
    TVD_r = vargh(3:end);
    TVD_l = vargh(1:end-2);
    
    % compute fluxes for TVD
    F_rl = .5*((1+theta(2:end-1)).*vel.*var + (1-theta(2:end-1)).*vel.*TVD_r);
    F_rh = .5*vel.*(var + TVD_r) - .5*vel.*vel.*dt/dz.*(TVD_r-var);
    
    F_ll = .5*((1+theta(2:end-1)).*vel.*TVD_l + (1-theta(2:end-1)).*vel.*var);
    F_lh = .5*vel.*(TVD_l+var) - .5*vel.*vel.*dt/dz.*(var-TVD_l);
    
    % do the job
    F_right = F_rl + phi_r.*(F_rh - F_rl);
    F_left  = F_ll + phi_l.*(F_lh - F_ll);
    
    vari = var - dt*(F_right-F_left)/dz;
    
    
    if any(~isreal(vari))
        disp('imaginary number = time step problem?')
        params.iterate = 0;
        return
    end

else
    xi = x - U*dt - w;
    xi(xi>max(x))  = max(x);
    xi(xi<min(x))  = min(x);
    vari = interp1(x,var,xi,method)';
end
end

%========================================================================
function vari = diffusion_1d(var,dz,dt)
%========================================================================
global NN
A = sparse(diag((1-dt/(dz*dz))*ones(NN,1)) +...
    diag(.5*dt/(dz*dz)*ones(NN-1,1),1) +...
    diag(.5*dt/(dz*dz)*ones(NN-1,1),-1));
% boundary conditions
% top zero gradient
% A(1,2)     = 2*A(1,2);
% top fixed value
A(1,1)     = 1;
A(1,2)     = 0;
% bottom fixed value
A(NN,NN)   = 1;
A(NN,NN-1) = 0;
vari = A*var;
end

%========================================================================
function Ts = get_Ts(P,F)
%========================================================================
global NN params;

adiabatic     = params.alpha*params.T0/(params.rhom*params.Cp);
dTdF_P        = params.dTdF_Ps*ones(NN,1);
dTdF_P(F>0.2) = params.dTdF_Ph;
Ts            = params.Ts0+dTdF_P.*F+(params.dTdP_F+adiabatic)*P;
end

%========================================================================
function [T,dF,latent_heat,Xwater] = melting(T,P,F,Xwater)
%========================================================================
global NN params;

% Solidus
Ts_dry     = get_Ts(P,F);
Ts_wet     = Ts_dry + params.aX.*(0.01.*Xwater).^params.bX;

% Calculate melt due to temperature > Ts
dTdF        = params.dTdF_Ps*ones(NN,1);
dTdF(F>0.2) = params.dTdF_Ph;

ind         = Xwater > 1e-6;
if any(ind)
    % Calculate the water effect on the slope of dTs_dF, i.e. dTs_dF(X)
    % 1) slope (derivative) of X(F)
    % dXdF   = -Xwater(ind).*(1./Dwater-1).*(1-F(ind)).^(1./Dwater-2);
    dXdF    = -Xwater(ind).*(1/params.Dbulk-1).*(1-F(ind)).^(1/params.Dbulk-2);
    % 2) slope of Ts(X)
    dTdX   = params.bX*params.aX.*(0.01.*Xwater(ind)).^(params.bX-1);
    % 3) combined to get slope of Ts( F(X) )
    dTdF_X = dTdX.*dXdF;
    % Add water effect to depletion dependence
    dTdF(ind,1) = dTdF(ind,1) + dTdF_X;
end

latent_heat      = params.T0*T*params.deltaS/params.Cp;
deltaT           = params.T0*T - Ts_wet;
deltaT(deltaT<0) = 0;
dF               = deltaT./(latent_heat + dTdF);

% update the water composition assuming a simple mass balance
Xwater = Xwater./(1 + dF.*(1./params.Dbulk - 1));

end

%========================================================================
function [T,dF,F,phi,Xwater] = meltingRK(T,P,F,phi,Xwater,dt)
%========================================================================
global NN params;

% Solidus
Ts_dry = get_Ts(P,F);
Ts_wet = Ts_dry + params.aX.*(0.01.*Xwater).^params.bX;

% Calculate melt due to temperature > Ts
dTdF        = params.dTdF_Ps*ones(NN,1);
dTdF(F>0.2) = params.dTdF_Ph;

ind         = Xwater > 1e-6;
if any(ind)
    % Calculate the water effect on the slope of dTs_dF, i.e. dTs_dF(X)
    % 1) slope (derivative) of X(F)
    % dXdF   = -Xwater(ind).*(1./Dwater-1).*(1-F(ind)).^(1./Dwater-2);
    dXdF    = -Xwater(ind).*(1/params.Dbulk-1).*(1-F(ind)).^(1/params.Dbulk-2);
    % 2) slope of Ts(X)
    dTdX   = params.bX*params.aX.*(0.01.*Xwater(ind)).^(params.bX-1);
    % 3) combined to get slope of Ts( F(X) )
    dTdF_X = dTdX.*dXdF;
    % Add water effect to depletion dependence
    dTdF(ind,1) = dTdF(ind,1) + dTdF_X;
end

L0               = dt*params.rhom*params.g*abs(params.vel);
latent_heat      = L0.*params.T0*T*params.deltaS/params.Cp;
deltaT           = params.T0*T - Ts_wet;
deltaT(deltaT<0) = 0;
dF01             = 0.5*deltaT./(latent_heat + dTdF);
F  = F + dF01;
T1 = params.T0*T - latent_heat.*dF01;

% update the water composition assuming a simple mass balance
Xwater = Xwater./(1 + dF01.*(1./params.Dbulk - 1));

% Solidus
Ts_dry = get_Ts(P,F);
Ts_wet = Ts_dry + params.aX.*(0.01.*Xwater).^params.bX;

dTdF        = params.dTdF_Ps*ones(NN,1);
dTdF(F>0.2) = params.dTdF_Ph;
ind         = Xwater > 1e-6;
if any(ind)
    % Calculate the water effect on the slope of dTs_dF, i.e. dTs_dF(X)
    % 1) slope (derivative) of X(F)
    % dXdF   = -Xwater(ind).*(1./Dwater-1).*(1-F(ind)).^(1./Dwater-2);
    dXdF    = -Xwater(ind).*(1/params.Dbulk-1).*(1-F(ind)).^(1/params.Dbulk-2);
    % 2) slope of Ts(X)
    dTdX   = params.bX*params.aX.*(0.01.*Xwater(ind)).^(params.bX-1);
    % 3) combined to get slope of Ts( F(X) )
    dTdF_X = dTdX.*dXdF;
    % Add water effect to depletion dependence
    dTdF(ind,1) = dTdF(ind,1) + dTdF_X;
end

latent_heat        = L0.*T1*params.deltaS/params.Cp;
deltaT1            = T1 - Ts_wet;
deltaT1(deltaT1<0) = 0;
dF12               = 2*deltaT1./(latent_heat + dTdF);
F                  = F + dF12;
T                  = (T1 - latent_heat.*dF12)/params.T0;

% update the water composition assuming a simple mass balance
Xwater = Xwater./(1 + dF12.*(1./params.Dbulk - 1));

dF = dF01 + dF12;
phi = phi + dF;

end




%========================================================================
% FLEXURE
%========================================================================
function [] = setupFlexProblem()
%========================================================================
global NNflex NEflex NENflex X dx Xmid

% Numerical parameters
NNflex      = 101;
NEflex      = NNflex-1;
NENflex     = 2;
X           = [0:1/(NEflex):1];
dx          = X(2)-X(1);
Xmid        = [1/(NEflex):1/(NEflex):1]-.5*dx;

end

%========================================================================
function [] = setupFlexParams(tstep,Te,eta,width)
%========================================================================
global params;

year        = 60*60*24*365;
Eyoungs     = 1e11;
Telastic    = Te;
poissons    = 0.25;
k           = (params.rhom-params.rhoi)*params.g;
%eta         = 1e22;           % Pa.s
tau         = 3*k*eta/Eyoungs; % Pa.s/m (Sleep & Snell, 1976)
D           = Eyoungs*Telastic^3/(12*(1-poissons^2));
params.lx   = 1000e3; % m

% Dimensionless numbers
params.tscale      = tau/k;
params.D0          = D/(k*params.lx^4);
params.q0          = 1/(k*params.lx);

halfwidth          = width/2; % m
params.lambdaload  = halfwidth/params.lx;

end

%========================================================================
function [] = setupFlexLocMatrix()
%========================================================================
global NEflex dx K_loc Kw_loc Ew_loc nodesflex params
% Setup element matrix
K_loc   =  params.D0*[ 12/dx^3  6/dx^2 -12/dx^3  6/dx^2;...
                6/dx^2  4/dx    -6/dx^2  2/dx  ;...
              -12/dx^3 -6/dx^2  12/dx^3 -6/dx^2;...
                6/dx^2  2/dx    -6/dx^2  4/dx];     % after Smith and Griffiths
Kw_loc  = dx/420*[156     22*dx    54    -13*dx;...
                     22*dx   4*dx^2  13*dx  -3*dx^2;...
                     54     13*dx   156    -22*dx;... %change 13*dx to -13*dx and check accuracy
                    -13*dx  -3*dx^2 -22*dx   4*dx^2];
Ew_loc  = dx/420*[156     22*dx    54    -13*dx;...
                    22*dx   4*dx^2  13*dx  -3*dx^2;...
                    54     13*dx   156    -22*dx;... %change 13*dx to -13*dx and check accuracy
                    -13*dx  -3*dx^2 -22*dx   4*dx^2];
                               
% Node numbering
for i=1:NEflex
    iel = i;
    nodesflex(iel,1) = i;
    nodesflex(iel,2) = i+1;
end
end

%========================================================================
function [w,w_out] = flexure(w,load,dtflex)
%========================================================================
global NNflex NEflex NENflex dx Xmid nodesflex K_loc Kw_loc Ew_loc params
    
    KK  = zeros(NNflex*2,NNflex*2);
    MM  = zeros(NNflex*2,NNflex*2);
%     MM = sparse(start_diag(1:NNflex*2,1:NNflex*2));
%     KK = sparse(start_diag(1:NNflex*2,1:NNflex*2));
    
    F   = zeros(NNflex*2,1);
    
    % shape of our ice sheet
    q = zeros(1,NEflex);
    halfwidth = round(params.lambdaload*NEflex);
    q(.5*(NEflex)-halfwidth:.5*(NEflex)+halfwidth) = params.q0*load;
    
    % Setup system matrix
    for iel=1:NEflex
        F_loc   = q(iel)*[dx/2 dx^2/12 dx/2 -dx^2/12];
        for i=1:NENflex
            ii                              = nodesflex(iel,i);
            for j=1:NENflex
                jj                          = nodesflex(iel,j);
                KK(ii*2-1,jj*2-1) = KK(ii*2-1,jj*2-1) + (K_loc(i*2-1,j*2-1) + Kw_loc(i*2-1,j*2-1));
                KK(ii*2  ,jj*2-1) = KK(ii*2  ,jj*2-1) + (K_loc(i*2  ,j*2-1) + Kw_loc(i*2  ,j*2-1));
                KK(ii*2,  jj*2)   = KK(ii*2,  jj*2)   + (K_loc(i*2,  j*2)   + Kw_loc(i*2,  j*2)  );
                KK(ii*2-1,jj*2)   = KK(ii*2-1,jj*2)   + (K_loc(i*2-1,j*2)   + Kw_loc(i*2-1,j*2)  );
                
                MM(ii*2-1,jj*2-1) = MM(ii*2-1,jj*2-1) + Ew_loc(i*2-1,j*2-1);
                MM(ii*2  ,jj*2-1) = MM(ii*2  ,jj*2-1) + Ew_loc(i*2  ,j*2-1);
                MM(ii*2,  jj*2)   = MM(ii*2,  jj*2)   + Ew_loc(i*2,  j*2)  ;
                MM(ii*2-1,jj*2)   = MM(ii*2-1,jj*2)   + Ew_loc(i*2-1,j*2)  ;
            end
            F(ii*2-1)             = F(ii*2-1)         + F_loc(i*2-1);
            F(ii*2)               = F(ii*2)           + F_loc(i*2);
        end
    end
    
    F_tot  = F + 1/dtflex*MM*w;
    KK_TOT = 1/dtflex*MM + KK;
    
    % Boundary conditions
    KK_TOT(1,:)     = 0; KK_TOT(1,1)        = 1; F_tot(1)       = 0; % Left  boundary
    KK_TOT(2,:)     = 0; KK_TOT(2,2)        = 1; F_tot(2)       = 0; % Left  boundary
    KK_TOT(end,:)   = 0; KK_TOT(end,end)    = 1; F_tot(end)     = 0; % Right boundary
    KK_TOT(end-1,:) = 0; KK_TOT(end-1,end-1)= 1; F_tot(end-1)   = 0; % Right boundary
        
    % Solve matrix
    w     = KK_TOT\F_tot;
    w_out = params.lx*w; % in meters!
    
end





%========================================================================
% MELT COMPOSITION
%========================================================================
function [Rcomp,Mcomp,num] = setupComp(NX)
%========================================================================
% Solid mantle composition
global NN
num    = 17;
Mcomp  = zeros(num,NN,NX);
%         La     Ce     Pr     Nd     Sm     Eu     Gd     Tb     Dy     Ho     Er     Tm     Yb     Lu     C      Nb     Zr
Rcomp1 = [0.206; 0.772; 0.143; 0.815; 0.299; 0.115; 0.419; 0.077; 0.525; 0.120; 0.347; 0.054; 0.347; 0.054; 285; 0.0;   0.0];
Prim   = [0.687; 1.775; 0.276; 1.354; 0.444; 0.168; 0.596; 0.108; 0.737; 0.164; 0.480; 0.074; 0.493; 0.074; 285; 0.86;   8.4];
Depl   = [0.234; 0.772; 0.131; 0.713; 0.270; 0.107; 0.395; 0.075; 0.533; 0.122; 0.371; 0.060; 0.401; 0.063; 285; 0.390; 7.190];
NMORB  = [2.500; 7.500; 1.320; 7.300; 2.630; 1.020; 3.680; 0.670; 4.550; 1.010; 2.970; 0.456; 3.050; 0.455; 285; 2.300; 74.00];
Rcomp  = repmat(0.6*Prim+0.4*NMORB,[1 NN NX]);

end

%========================================================================
function [CMantle,CMelt,D] = REEcomp(T,P,coord,dF,CMantle,CMelt,phi)
%========================================================================
% Calculate melt composition
global NN params

num   = 17;
D     = zeros(num,NN);     % Array to store the partition coefficients

indf = find(dF > 0);

% Partition coefficents for the REEs
% Gibson & Geist Supp. Matt. Earth and Planetary Science Letters
% 2010 (+ McK & O'N for plag and spinel)
% Rosenthal et al.,  Earth and Planetary Science Letters 2015 for Carbon
% Gurenko & Chaussidon (1995) for Nb and Zr
Dol     = [0.0005 0.0005 0.0008 0.00042 0.0011 0.0016 0.0011...
    0.0015 0.0027 0.0016 0.013 0.0015  0.02  0.02  0.0007  0.00042 0.00058];
Dopx    = [0.0031 0.004  0.0048 0.012   0.02   0.013  0.0065...
    0.019  0.011  0.026  0.045 0.04    0.08  0.12  0.0003  0.00052 0.00330];
Dcpx    = [.049   0.08   0.126  0.178   0.293  0.335  0.35...
    0.403  0.400  0.427  0.420 0.40968 0.400 0.376 0.0005  0.18730 0.12340];
Dplag   = [0.27   0.20   0.17   0.14    0.11   0.73   0.066...
    0.06   0.055  0.048  0.041 0.036   0.031 0.025 0.00055 0.06520 0.00090];
Dspinel = [0.01   0.01   0.01   0.01    0.01   0.01   0.01...
    0.01   0.01   0.01   0.01  0.01    0.01  0.01 0.00055  0.00060 0.07000];
Dgarnet = [0.001  0.005  0.014  0.052   0.250  0.496  0.848...
    1.477  2.200  3.315  4.400 5.495   6.600 7.100 0.0001  0.05700 0.50500];
Damph   = [0.17   0.26   0.35   0.44    0.76   0.88   0.86...
    0.83   0.78   0.73   0.68  0.64    0.59  0.51 0.00055  0.0    0.0];

% Proportions of minerals in each facies (olovine, opx, cpx and
% then plagiocalse, spinel, garnet or amphibol).
Plag   = [0.636 0.263 0.012 0.089];
Spinel = [0.578 0.270 0.119 0.033];
Garnet = [0.598 0.211 0.079 0.115];
Amph   = [0.599 0.247 0.038 0.116];

% lithostatic pressure (in GPa) but we could use dynamic...
P      = 1e-9*P;

% how deep are we (in km)?
ssz    = 1e-3*params.lz*coord';

% Spinel-out and garnet-in boundaries
Pspinel_out = (params.T0*T+400)/666.7;
Pgarnet_in  = (params.T0*T+533)/666.7;

% Shallow first: if < 25 km  deep we are in the plagioclase
% stability field
[~,nb] = size(D(:,ssz<=25));
D(:,ssz<=25) = repmat(Dol.*Plag(1) + Dopx.*Plag(2) + Dcpx.*Plag(3) + Dplag.*Plag(4),nb,1)';

%  if between stability, linear transition... %
Da   = zeros(num,1);
Db   = zeros(num,1);
m    = zeros(num,1);
c    = zeros(num,1);
Da = Dol.*Plag(1) + Dopx.*Plag(2) + Dcpx.*Plag(3) + Dplag.*Plag(4);
Db = Dol.*Spinel(1) + Dopx.*Spinel(2) + Dcpx.*Spinel(3) + Dspinel.*Spinel(4);
m  = (Db-Da)./10;
c  = Da-m*25;
for i = 1:num
    D(i,ssz>25 & ssz<=35)  = m(i).*ssz(ssz>25 & ssz<=35)+c(i);
end

% if > 35 km and P < spinel-out
[~,nb] = size(D(:,ssz>35 & P<=Pspinel_out));
D(:,ssz>35 & P<=Pspinel_out) = repmat(Dol.*Spinel(1) + Dopx.*Spinel(2) + Dcpx.*Spinel(3) + Dspinel.*Spinel(4),nb,1)';

% again between stability, linear transition... %
Da   = zeros(num,1);
Db   = zeros(num,1);
m    = zeros(1,NN);
c    = zeros(1,NN);
Da = Dol.*Spinel(1) + Dopx.*Spinel(2) + Dcpx.*Spinel(3) + Dspinel.*Spinel(4);
Db = Dol.*Garnet(1) + Dopx.*Garnet(2) + Dcpx.*Garnet(3) + Dgarnet.*Garnet(4);
for i = 1:num
    m = (Db(i)-Da(i))./(Pgarnet_in-Pspinel_out);
    c = Da(i)-m.*Pspinel_out;
    D(i,P>Pspinel_out & P<=Pgarnet_in) = m(P>Pspinel_out & P<=Pgarnet_in).*P(P>Pspinel_out & P<=Pgarnet_in)+c(P>Pspinel_out & P<=Pgarnet_in);
end

% if P > granet_in
[~,nb] = size(D(:,P>Pgarnet_in));
D(:,P>Pgarnet_in) = repmat(Dol.*Garnet(1) + Dopx.*Garnet(2) + Dcpx.*Garnet(3) + Dgarnet.*Garnet(4),nb,1)';

for i = 1:num
    % calculate melt composition from the solid composition
    % CMelt(i,indf) = CMantle(i,indf)./D(i,indf);
    % update the solid composition assuming a simple mass balance
    % melt_prod_rate = dF./dt;
    % CMantle(i,indf) = CMantle(i,indf)./(1 + dF(indf)'.*(1./D(i,indf) - 1));
    
    % try Spiegelman 1996 dissequilibrium
    CMantle(i,indf) = CMantle(i,indf)./(1 + (dF(indf)'./(1-phi(indf)')).*(1./D(i,indf) - 1));
    CMelt(i,indf) = CMantle(i,indf)./D(i,indf).*(dF(indf)'./(phi(indf)')) + CMelt(i,indf).*(1-(dF(indf)'./(phi(indf)')));
    
end
% and make sure solid composition is not less than zero
CMantle(CMantle<0) = 0;

end

%========================================================================
function [vz] = corner_flow(v,xcoord,zcoord,lx,lz,nz)
%========================================================================

theta = linspace(0,pi/2,nz);
r     = linspace(0,1.5,nz);
x     = zeros(length(r),length(theta));
z     = zeros(length(r),length(theta));
psi   = zeros(length(r),length(theta));

disp(v)

angle = 45;
alpha = angle*pi/180;

A = 2*sin(alpha)^2/(pi-2*alpha-sin(2*alpha));
D = 2/(pi-2*alpha-sin(2*alpha));

for i = 1:length(theta)
    for j = 1:length(r)
        x(j,i)   = r(j)*sin(theta(i));
        z(j,i)   = r(j)*cos(theta(i));
        psi(j,i) = v*r(j)*(A*sin(theta(i)) - D*theta(i)*cos(theta(i)));
    end
end

[XX,ZZ] = meshgrid(xcoord*lx/lz,zcoord);
PSI     = griddata(x(:),z(:),psi(:),XX,ZZ);

dx = min(diff(xcoord))*lx/lz;
dz = min(diff(zcoord));
[vz,vx]  = gradient(PSI,dx,dz);
vz(vz>0) = 0;

end