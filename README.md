# melt1d-icesheet

Code developed by John Armitage (IPGP)

The repository contains two MATLAB scripts and one matlab data file:

run_ice.m : This script runs the function melt1d_TVD_ice.

melt1d_TVD_ice.m : This script contains the function melt_TVD_ice, which solves the sets of equations described in the supplementary.pdf.

iceland_rescaled_120k.mat : This data file contians ice sheet histories for Iceland covering the last 120 kyrs.

The codes here are working codes. I have tried my best to make them ledgible and commented. It can be broken down into four parts:

(1) Flexure due to an ice sheet, where the flexure equation is solved using a simple finite element scheme.

(2) A vertical melting column, where the advection equation is solved using a finite volume total variance diminishing scheme and diffusion solved using a simple finite difference scheme.

(3) Melt production using kinetic relationships and a Runga Kutta scheme.

(4) Melt composition following disequilibrium melting where the partition coefficient is calculated from the mineral phases present.

If you want to run this code, and have no idea how then write me an e-mail (armtiage@ipgp.fr)