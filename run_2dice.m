clear all
up = [10e-3];
for i = 1:length(up)
    [time,glacier,mprd,merupt,incCMelt,topCMelt,carbonerupt,Fmean,Fmax,phi,T,zcoord,xcoord,ice] = melt2d_TVD_ice(10e3,10^21,200e3,1450,up(i));
    filename = sprintf('data/iceland/2D_1e-5_1450_800.mat',1450,up(i));
    save(filename)
end






